import math
import sys
import turtle
 
def drawCircle(self, x, y, radius=50):
        """
        Moves the turtle to the correct position and draws a circle
        """
        self.penup()
        self.setposition(x, y)
        self.pendown()
        self.circle(radius)

def drawTracklet(self, x, y, angle):
        """
        Moves the turtle to the correct position and draws a circle
        """
        self.penup()
        self.setposition(x, y)
        self.pendown()
        self.left(angle)
	self.forward(40)
	self.back(80)
	self.right(angle)

def rescaleAndShift(hits, minimum_value, scaling, shift):
   for i_hit,hit in enumerate(hits):
       hits[i_hit] = (hit - minimum_value) * scaling - shift
   return hits

mdtHits = open(sys.argv[1])
tracklets = open(sys.argv[2])
  
hitR = []
hitZ = []
hitRadius = []

smallest_R = 99999999
smallest_z = 99999999

turtle.radians()
turtle.hideturtle()
turtle.setup(1600,1000,0,0)
turtle.tracer(0, 0)

print turtle.window_width(),' ', turtle.window_height()

for line in mdtHits:
    line = line.replace(' ', '')
    data = line.split("*")
    tmp_r = float(data[1])#math.sqrt(float(data[1])**2 + float(data[2])**2)
    tmp_z = float(data[2])
    hitR.append(tmp_r)

    if tmp_r < smallest_R:
	smallest_R = tmp_r
    if tmp_z < smallest_z:
	smallest_z = tmp_z

    hitZ.append(tmp_z)
    hitRadius.append(0.75*float(data[3]))

hitR = rescaleAndShift(hitR, smallest_R, 0.75, 200)
hitZ = rescaleAndShift(hitZ, smallest_z, 0.75, 700)

ml1_r = []
ml1_z = []
ml1_a = []
ml2_r = []
ml2_z = []
ml2_a = []

for line in tracklets:
    line = line.replace(' ', '')
    data = line.split("*")
    tmp_r = math.sqrt(math.pow(float(data[0]),2) + math.pow(float(data[1]),2))
    ml1_r.append(tmp_r)
    ml1_z.append(float(data[2]))
    ml1_a.append(float(data[3]))
    tmp_r = math.sqrt(math.pow(float(data[4]),2) + math.pow(float(data[5]),2))
    ml2_r.append(tmp_r)
    ml2_z.append(float(data[6]))
    ml2_a.append(float(data[7]))

ml1_r = rescaleAndShift(ml1_r, smallest_R, 0.75, 200)
ml1_z = rescaleAndShift(ml1_z, smallest_z, 0.75, 700)
ml2_r = rescaleAndShift(ml2_r, smallest_R, 0.75, 200)
ml2_z = rescaleAndShift(ml2_z, smallest_z, 0.75, 700)

#for r,z in zip(ml1_r,ml1_z):
#	pt = Point(z,r)
#	pt.draw(win)

for i_hit,r in enumerate(hitR):
	drawCircle(turtle, hitZ[i_hit],r,hitRadius[i_hit])

turtle.update()
turtle.pencolor('blue')
turtle.width(2)
for i_t in range(0,len(ml1_r)-1):
	drawTracklet(turtle, ml1_z[i_t], ml1_r[i_t], ml1_a[i_t])
	drawTracklet(turtle, ml2_z[i_t], ml2_r[i_t], ml2_a[i_t])

turtle.update()
